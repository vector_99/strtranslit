Транслит
========

[![Crates.io](https://img.shields.io/badge/license-mit-brightgreen.svg)](LICENSE.md)

Расширение для траслита кириллицы в латиницу.
Можно использовать например для создания URL

> Note: данное расширение создавалось исключительно для моих проектов.

> Используя его в Ваших проектах - Вы делаете это на свой страх и риск

Установка
---------

Установка с помощью [composer](http://getcomposer.org/download/).

Запустить комманду

```
php composer.phar require --prefer-dist vector99/yii2-strtranslit "~1.0.0"
```

или добавить следующую строку в файл `composer.json`

```
"vector99/yii2-strtranslit": "~1.0.0"
```

Использование
-------------

```php
use vector99\helpers\Strtranslit;

// вывод: kakoy-to-dlinnyy-tekst-1
echo Strtranslit::translIt('Какой то длинный текст №1');
```